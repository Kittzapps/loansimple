package com.loanSimple.sampleapp.networkClient.gateway

import com.loanSimple.sampleapp.networkClient.GetApis
import javax.inject.Inject

class NetworkInteractor @Inject constructor(var newtworkGateWay: NetworkGateWay) {
    fun getApiClient(): GetApis {
        return newtworkGateWay.getRetrofit()
    }
}