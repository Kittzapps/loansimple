package com.loanSimple.sampleapp.models.response


import com.google.gson.annotations.SerializedName

data class ResultsItem(
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("created_at")
    val createdAt: String = "",
    @SerializedName("updated_at")
    val updatedAt: String = "",
    @SerializedName("upi_txn_id")
    val upiTxnId: String = "",
    @SerializedName("merchant_ref_no")
    val merchantRefNo: String = "",
    @SerializedName("type")
    val type: String = "",
    @SerializedName("amount")
    val amount: String = "",
    @SerializedName("txn_auth_date")
    val txnAuthDate: String = "",
    @SerializedName("status")
    val status: String = "",
    @SerializedName("status_desc")
    val statusDesc: String = "",
    @SerializedName("response_code")
    val responseCode: String = "",
    @SerializedName("error_code")
    val errorCode: Any? = null,
    @SerializedName("approval_no")
    val approvalNo: String = "",
    @SerializedName("payer_vpa")
    val payerVpa: String = "",
    @SerializedName("npci_upi_txn_id")
    val npciUpiTxnId: String = "",
    @SerializedName("ref_id")
    val refId: Any? = null,
    @SerializedName("payer_mobile_no")
    val payerMobileNo: String = "",
    @SerializedName("payee_mobile_no")
    val payeeMobileNo: Any? = null,
    @SerializedName("payer_note")
    val payerNote: String = "",
    @SerializedName("customer_ref_id")
    val customerRefId: String = "",
    @SerializedName("payer_bank_account_no")
    val payerBankAccountNo: String = "",
    @SerializedName("payer_bank_ifsc")
    val payerBankIfsc: String = "",
    @SerializedName("payer_bank_account_holder_name")
    val payerBankAccountHolderName: String = "",
    @SerializedName("payee_vpa")
    val payeeVpa: String = "",
    @SerializedName("payee_bank_ifsc")
    val payeeBankIfsc: String = "",
    @SerializedName("payee_bank_account_no")
    val payeeBankAccountNo: String = "",
    @SerializedName("payee_aadhaar")
    val payeeAadhaar: String = "",
    @SerializedName("payee_name")
    val payeeName: String = "",
    @SerializedName("fund_transfer")
    val fundTransfer: Int = 0
)


data class UserUpiTranscationModel(
    @SerializedName("count")
    val count: Int = 0,
    @SerializedName("next")
    val next: Any? = null,
    @SerializedName("previous")
    val previous: Any? = null,
    @SerializedName("results")
    val results: List<ResultsItem>?
)


