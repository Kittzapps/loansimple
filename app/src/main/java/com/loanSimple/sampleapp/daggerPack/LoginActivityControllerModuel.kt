package com.loanSimple.sampleapp.daggerPack

import com.loanSimple.sampleapp.gateway.activityGateWay.LoginActivityGateWay
import com.loanSimple.sampleapp.gatewayImpl.activityGatewayImpl.LoginActivityGatewayImpl
import dagger.Module
import dagger.Provides

@Module
class LoginActivityControllerModuel {
    @Provides
    fun getLoginActivityGateway():LoginActivityGateWay{
        return LoginActivityGatewayImpl()
    }
}