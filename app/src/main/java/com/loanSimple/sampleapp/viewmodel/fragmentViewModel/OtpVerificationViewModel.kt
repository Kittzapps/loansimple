package com.loanSimple.sampleapp.viewmodel.activityViewModel.fragmentViewModel

import com.loanSimple.sampleapp.models.response.OtpVerificationResponseModel
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class OtpVerificationViewModel @Inject constructor() {
    val otpVerificationObservable: BehaviorSubject<OtpVerificationResponseModel> = BehaviorSubject.create()

    fun getOtpObservable(): Observable<OtpVerificationResponseModel> {
        return otpVerificationObservable
    }

    fun setOtpObservable(otpVerificationResponseModel: OtpVerificationResponseModel) {
        otpVerificationObservable.onNext(otpVerificationResponseModel)
    }

}