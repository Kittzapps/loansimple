package com.loanSimple.sampleapp.gateway

import io.reactivex.Observable
import java.util.*

interface DateListDataGateway {
    fun getDates(startDate: Date, endDate: Date): Observable<List<Date>>
}
