package com.loanSimple.sampleapp.networkClient.gateway

import com.loanSimple.sampleapp.networkClient.GetApis


interface NetworkGateWay {
    fun getRetrofit(): GetApis
}