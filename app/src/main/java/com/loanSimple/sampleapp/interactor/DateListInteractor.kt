package com.loanSimple.sampleapp.interactor

import com.loanSimple.sampleapp.gateway.DateListDataGateway
import com.loanSimple.sampleapp.utils.DateUtility
import io.reactivex.Observable
import java.util.*
import javax.inject.Inject

class DateListInteractor @Inject constructor(private val dateListDataGateway: DateListDataGateway) {
    fun getDateList(): Observable<List<Date>> {
        return dateListDataGateway.getDates(DateUtility.getCurrentDate(), DateUtility.getOneMonthDates())
    }

}