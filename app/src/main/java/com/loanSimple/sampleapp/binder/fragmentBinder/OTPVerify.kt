package com.loanSimple.sampleapp.binder.fragmentBinder

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Guideline
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.loanSimple.sampleapp.R
import com.loanSimple.sampleapp.binder.activityBinder.TransactionActivity
import com.loanSimple.sampleapp.controller.activityController.fragmentController.OtpVerificationController
import com.loanSimple.sampleapp.daggerPack.DaggerOtpVerificationComponent
import com.loanSimple.sampleapp.models.response.OtpVerificationResponseModel
import com.loanSimple.sampleapp.utils.Logger
import com.loanSimple.sampleapp.viewmodel.activityViewModel.fragmentViewModel.OtpVerificationViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.otp_screen.*
import kotlinx.android.synthetic.main.otp_verify_content.*
import kotlinx.android.synthetic.main.otp_verify_custom_dialog.*
import javax.inject.Inject


class OTPVerify : Fragment(), TextWatcher {
    @Inject
    lateinit var otpVerificationController: OtpVerificationController
    @Inject
    lateinit var otpVerificationViewModel: OtpVerificationViewModel
    lateinit var number: String
    lateinit var disposable: Disposable
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        number = arguments?.getString("number").toString()
    }

    override fun afterTextChanged(editable: Editable?) {
        if (editable!!.length == 1) {
            if (otp_one.length() == 1) {
                otp_two.requestFocus()
            }
            if (otp_two.length() == 1) {
                otp_three.requestFocus()
            }
            if (otp_three.length() == 1) {
                otp_four.requestFocus()
            }
            if (otp_four.length() == 1) {
                otp_five.requestFocus()
            }
            if (otp_five.length() == 1) {
                otp_six.requestFocus()
            }
            if (otp_six.length() == 1) {
                otp_six.isCursorVisible = false
                otpVerificationController.sendOtpRequest(
                    otp = getOtpString(),
                    mobile = number,
                    viewModel = otpVerificationViewModel
                )
            }
        } else if (editable.isEmpty()) {
            if (otp_six.length() == 0) {
                otp_five.requestFocus()
            }
            if (otp_five.length() == 0) {
                otp_four.requestFocus()
            }
            if (otp_four.length() == 0) {
                otp_three.requestFocus()
            }
            if (otp_three.length() == 0) {
                otp_two.requestFocus()
            }
            if (otp_two.length() == 0) {
                otp_one.requestFocus()
            }
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//
    }

    private lateinit var guideLine: Guideline
    lateinit var params: ConstraintLayout.LayoutParams
    lateinit var dialog: Dialog


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.otp_screen, container, false)
        guideLine = view.findViewById(R.id.guideline)
        dialog = Dialog(context!!, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen)
        return view
    }

    override fun onStart() {
        super.onStart()
        setListenerEdittext()
        changeTextColor()
        initController()
        observeOtpVerification()
    }

    private fun initController() {
        otpVerificationController = DaggerOtpVerificationComponent.create().otpVerificationController()
        otpVerificationViewModel = DaggerOtpVerificationComponent.create().otpVerificationViewModel()
    }

    private fun dialogOpen(
        otp: String,
        otpVerifyResponse: OtpVerificationResponseModel
    ) {

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.otp_verify_custom_dialog)
        dialog.otp_number_TextView.text = otp
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.cardView.setBackgroundResource(R.drawable.circle)
        dialog.show()

        dialog.otpVerifyDoneButton.setOnClickListener {
            val intentdd = Intent(activity!!, TransactionActivity::class.java)
            val bund = Bundle()
            bund.putSerializable("resp", otpVerifyResponse)
            intentdd.putExtras(bund)
            (activity as AppCompatActivity).startActivity(intentdd)
            onDestroyView()
            activity?.finish()
        }
        dialog.setOnCancelListener {
            setLayoutParam(0.10f)
            back_button.visibility = View.VISIBLE
        }
        back_button.visibility = View.GONE
        setLayoutParam(0.0f)
        otpContainer.visibility = View.GONE
    }

    private fun setLayoutParam(percent: Float) {
        params = guideLine.layoutParams as ConstraintLayout.LayoutParams
        params.guidePercent = percent
        guideLine.layoutParams = params
    }

    private fun setListenerEdittext() {
        otp_one.addTextChangedListener(this)
        otp_two.addTextChangedListener(this)
        otp_three.addTextChangedListener(this)
        otp_four.addTextChangedListener(this)
        otp_five.addTextChangedListener(this)
        otp_six.addTextChangedListener(this)
    }

    private fun changeTextColor() {
        val builder = SpannableStringBuilder()
        val green = "Didn't receive OTP?"
        val greenSpannable = SpannableString(green)
        greenSpannable.setSpan(
            ForegroundColorSpan(
                Color.parseColor(
                    "#" + Integer.toHexString(
                        ContextCompat.getColor(
                            context!!,
                            R.color.colorDarkGreen
                        )
                    )
                )
            ), 0, green.length, 0
        )
        builder.append(greenSpannable)

        val red = " Resend"
        val redSpannable = SpannableString(red)
        redSpannable.setSpan(ForegroundColorSpan(Color.RED), 0, red.length, 0)
        builder.append(redSpannable)
        resend_otp.setText(builder, TextView.BufferType.SPANNABLE)

        back_button.setOnClickListener { activity!!.supportFragmentManager.popBackStack() }
    }

    private fun observeOtpVerification() {
        disposable = otpVerificationViewModel.getOtpObservable().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe { otpVerifyResponse ->
                Logger.d("BHANU", "VERIFY-----> $otpVerifyResponse")
                dialogOpen(getOtpString(), otpVerifyResponse)
            }
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }

    private fun getOtpString(): String {
        return "${otp_one.text}${otp_two.text}${otp_three.text}${otp_four.text}${otp_five.text}${otp_six.text}"
    }

    override fun onDestroyView() {
        if (dialog.isShowing)
            dialog.dismiss()
        super.onDestroyView()
    }


}
