package com.loanSimple.sampleapp.daggerPack

import com.loanSimple.sampleapp.networkClient.gateway.NetworkGateWay
import com.loanSimple.sampleapp.networkClient.gateway.NetworkGatewayImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkClientModule {
    @Provides
    fun getNetworkGateway(): NetworkGateWay {
        return NetworkGatewayImpl()
    }
}