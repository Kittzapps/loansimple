package com.loanSimple.sampleapp

import android.app.Application
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

class AppApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
            .setDefaultFontPath("font/quicksand_regular.ttf")
            .setFontAttrId(R.attr.fontPath)
            .build())
    }
}