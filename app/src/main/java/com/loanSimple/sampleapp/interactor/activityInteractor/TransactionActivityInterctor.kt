package com.loanSimple.sampleapp.interactor.activityInteractor

import com.loanSimple.sampleapp.gateway.activityGateWay.TransactionActivityGateWay
import com.loanSimple.sampleapp.models.response.UserUpiTranscationModel
import io.reactivex.Observable
import javax.inject.Inject

class TransactionActivityInterctor @Inject constructor(private var transactionActivityGateWay: TransactionActivityGateWay) {
    fun getTransactionResponseFromGateway(accountId: Int, sessionId: String): Observable<UserUpiTranscationModel> {
        return transactionActivityGateWay.observeTransactionRequestResponse(accountId,sessionId)
    }
}