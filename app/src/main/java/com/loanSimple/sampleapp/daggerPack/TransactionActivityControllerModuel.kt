package com.loanSimple.sampleapp.daggerPack

import com.loanSimple.sampleapp.gateway.activityGateWay.TransactionActivityGateWay
import com.loanSimple.sampleapp.gatewayImpl.activityGatewayImpl.TransactionActivityGatewayImpl
import dagger.Module
import dagger.Provides

@Module
class TransactionActivityControllerModuel {
    @Provides
    fun getTransactionActivityGateway():TransactionActivityGateWay{
        return TransactionActivityGatewayImpl()
    }
}