package com.loanSimple.sampleapp.models.requestPayload

data class OtpVerificationPayload(val mobile: String,
                                  val type: String,
                                  val is_bypass_access: Boolean,
                                  val otp:String) {
}