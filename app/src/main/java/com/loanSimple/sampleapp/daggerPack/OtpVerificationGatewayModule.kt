package com.loanSimple.sampleapp.daggerPack

import com.loanSimple.sampleapp.gateway.fragmentGateway.OtpVerificationGateway
import com.loanSimple.sampleapp.gatewayImpl.activityGatewayImpl.fragmentGatewayImpl.OtpVerificationGatewayImpl
import dagger.Module
import dagger.Provides

@Module
class OtpVerificationGatewayModule {
    @Provides
    fun getOtpGateway(): OtpVerificationGateway {
        return OtpVerificationGatewayImpl()
    }
}