package com.loanSimple.sampleapp.controller.activityController

import com.loanSimple.sampleapp.viewmodel.activityViewModel.LoginActivityViewModel
import com.loanSimple.sampleapp.daggerPack.DaggerLoginActivityControllerComponent
import javax.inject.Inject

class LoginActivityController @Inject constructor() {
    private var loginActivityInterctor = DaggerLoginActivityControllerComponent.create().getLoginActivityInteractor()
    private var loginactivityPresenter = DaggerLoginActivityControllerComponent.create().getLoginActivityPresenter()

    fun sendOtpRequest(number: String,viewModel: LoginActivityViewModel) {
        loginactivityPresenter.setOtpRequestResponse(loginActivityInterctor.getResponseFromGateway(number),viewModel)
    }
}