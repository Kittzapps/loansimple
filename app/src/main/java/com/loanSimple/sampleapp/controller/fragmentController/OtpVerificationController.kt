package com.loanSimple.sampleapp.controller.activityController.fragmentController

import com.loanSimple.sampleapp.daggerPack.DaggerOtpVerificationControllerComponent
import com.loanSimple.sampleapp.viewmodel.activityViewModel.fragmentViewModel.OtpVerificationViewModel
import javax.inject.Inject

class OtpVerificationController @Inject constructor() {
    private var otpVerificationInterctor = DaggerOtpVerificationControllerComponent.create().getInteractor()
    private var otpVerificationPresenter = DaggerOtpVerificationControllerComponent.create().getPresenter()

    fun sendOtpRequest(otp: String,mobile: String,viewModel: OtpVerificationViewModel) {
        otpVerificationPresenter.setOtpVerificationRequestResponse(otpVerificationInterctor.getResponseFromOtpVerificationGateway(otp,mobile),viewModel)
    }
}