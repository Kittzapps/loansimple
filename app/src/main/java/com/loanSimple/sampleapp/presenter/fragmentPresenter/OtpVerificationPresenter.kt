package com.loanSimple.sampleapp.presenter.fragmentPresenter

import com.loanSimple.sampleapp.viewmodel.activityViewModel.fragmentViewModel.OtpVerificationViewModel
import com.loanSimple.sampleapp.models.response.OtpVerificationResponseModel
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class OtpVerificationPresenter @Inject constructor() {

    fun setOtpVerificationRequestResponse(
        responseFromOtpVerificationGateway: Observable<OtpVerificationResponseModel>,
        viewModel: OtpVerificationViewModel
    ) {
responseFromOtpVerificationGateway.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
    object : Observer<OtpVerificationResponseModel> {
        override fun onComplete() {

        }

        override fun onSubscribe(d: Disposable) {
        }

        override fun onNext(t: OtpVerificationResponseModel) {
            viewModel.setOtpObservable(t)
        }

        override fun onError(e: Throwable) {
        }
    })
    }
}