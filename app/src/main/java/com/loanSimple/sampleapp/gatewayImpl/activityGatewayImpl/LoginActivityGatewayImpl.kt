package com.loanSimple.sampleapp.gatewayImpl.activityGatewayImpl

import android.util.Log
import com.loanSimple.sampleapp.gateway.activityGateWay.LoginActivityGateWay
import com.loanSimple.sampleapp.daggerPack.DaggerNetworkComponnet
import com.loanSimple.sampleapp.models.requestPayload.RequestOtpPayload
import com.loanSimple.sampleapp.models.response.SendOtpModel
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class LoginActivityGatewayImpl @Inject constructor() : LoginActivityGateWay {
    override fun observeOtpRequestResponse(number: String): Observable<SendOtpModel> {
        return getOtpObservable(number)
    }

    private val behaviourSubject: BehaviorSubject<SendOtpModel> = BehaviorSubject.create()

    private fun setObservableData(sendOtpModel: SendOtpModel) {
        behaviourSubject.onNext(sendOtpModel)
    }

    private fun getOtpObservable(number: String): Observable<SendOtpModel> {
        return getResponseFromOtpRequest(number)
    }


    private fun getResponseFromOtpRequest(number: String): Observable<SendOtpModel> {
        Log.d("BHANU", "NUMBER::: $number")
        DaggerNetworkComponnet.create().getNetworkClient().getApiClient()
            .loginCall(RequestOtpPayload(number, "send_otp", true))
            .enqueue(object : Callback<SendOtpModel> {
                override fun onFailure(call: Call<SendOtpModel>, t: Throwable) {

                }

                override fun onResponse(call: Call<SendOtpModel>, response: Response<SendOtpModel>) {
                    if (response.body() != null) {
                        Log.d("BHANU", "INSIDE IMPL ACTIVITY---->")
                        setObservableData(response.body()!!)

                    }
                }

            })
        return behaviourSubject
    }

}