package com.loanSimple.sampleapp.utils

import android.util.Log
import com.loanSimple.sampleapp.BuildConfig

class Logger() {
    companion object {
        fun i(tag: String, message: String) {
            if (BuildConfig.DEBUG) {
                Log.i(tag,message)
            }

        }

        fun d(tag: String, message: String) {
            if (BuildConfig.DEBUG) {
                Log.d(tag,message)
            }
        }

        fun v(tag: String, message: String) {
            if (BuildConfig.DEBUG) {
                Log.v(tag,message)
            }
        }

        fun w(tag: String, message: String) {
            if (BuildConfig.DEBUG) {
                Log.w(tag,message)
            }
        }

        fun e(tag: String, message: String) {
            if (BuildConfig.DEBUG) {
                Log.e(tag,message)
            }
        }

        fun wtf(tag: String, message: String) {
            if (BuildConfig.DEBUG) {
                Log.wtf(tag,message)
            }
        }
    }
}