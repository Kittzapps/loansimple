package com.loanSimple.sampleapp.models.requestPayload

data class RequestOtpPayload(
    val mobile: String,
    val type: String,
    val is_bypass_access: Boolean
)