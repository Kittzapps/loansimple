package com.loanSimple.sampleapp.binder.activityBinder

import android.content.Context
import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.loanSimple.sampleapp.controller.activityController.TransactionActivityController
import com.loanSimple.sampleapp.daggerPack.DaggerHorizontalDateAdapterComponent
import com.loanSimple.sampleapp.daggerPack.DaggerTransactionActivityComponent
import com.loanSimple.sampleapp.daggerPack.DaggerTransactionHistoryAdapterComponent
import com.loanSimple.sampleapp.models.response.OtpVerificationResponseModel
import com.loanSimple.sampleapp.models.response.UserUpiTranscationModel
import com.loanSimple.sampleapp.utils.Logger
import com.loanSimple.sampleapp.viewmodel.activityViewModel.TransactionActivityViewModel
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_transaction.*
import kotlinx.android.synthetic.main.horizontal_date_recyclerview.*
import kotlinx.android.synthetic.main.transcation_history_recycler.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import java.util.*
import javax.inject.Inject


class TransactionActivity : AppCompatActivity() {
    @Inject
    lateinit var transactionActivityController: TransactionActivityController
    @Inject
    lateinit var transactionActivityViewModel: TransactionActivityViewModel
    lateinit var disposable: Disposable
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val otpBundle = intent?.extras
        val resp = otpBundle?.getSerializable("resp") as OtpVerificationResponseModel

        setContentView(com.loanSimple.sampleapp.R.layout.activity_transaction)
        profileImageContainer.setBackgroundResource(com.loanSimple.sampleapp.R.drawable.circle)


        initViews(resp)
        initDependencies(resp.data.pesimpleAccountId, resp.data.sessionid)

        observeTranscationData()
        observeDateListData()
    }

    private fun initDateRecycler() {
        dateRecyclerView.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
        dateRecyclerView.layoutManager = linearLayoutManager
    }


    private fun initTransactionRecycler() {
        transactionHistoryRecycler.setHasFixedSize(true)
        val linearLayoutManager1 = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        transactionHistoryRecycler.layoutManager = linearLayoutManager1
    }

    private fun initViews(response: OtpVerificationResponseModel) {
        bussinessNameTextView.text = response.data.businessName
        userNameTextView.text = response.data.personName
        initDateRecycler()
        initTransactionRecycler()
    }

    private fun initDependencies(pesimpleAccountId: Int, sessionid: String) {
        transactionActivityController = DaggerTransactionActivityComponent.create().getController()
        transactionActivityViewModel = DaggerTransactionActivityComponent.create().getViewModel()
        transactionActivityController.sendTransactionRequest(
            pesimpleAccountId,
            sessionid,
            transactionActivityViewModel
        )
        transactionActivityController.sendDateListRequest(transactionActivityViewModel)
    }


    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    private fun observeTranscationData() {
        transactionActivityViewModel.getTransactionResponseObservable().observeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                object : Observer<UserUpiTranscationModel> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {
                        disposable = d
                    }

                    override fun onNext(t: UserUpiTranscationModel) {
                        Logger.d("BHANU", "INSIDE ACTIVITY TRANS--------> $t")
                        transactionHistoryRecycler.adapter =
                            DaggerTransactionHistoryAdapterComponent.builder()
                                .getTransData(t.results!!)
                                .build()
                                .getTransactionAdapter()
                        disposable.dispose()


                    }

                    override fun onError(e: Throwable) {
                    }
                })
    }

    private fun observeDateListData() {
        transactionActivityViewModel.getDateListObservable().observeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                object : Observer<List<Date>> {
                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {
                        disposable = d
                    }

                    override fun onNext(t: List<Date>) {
                        Logger.d("BHANU", "INSIDE ACTIVITY TRANS--------> $t")
                        dateRecyclerView.adapter =
                            DaggerHorizontalDateAdapterComponent.builder()
                                .getListData(t)
                                .build()
                                .getDateAdapter()
                        if (!disposable.isDisposed) {
                            disposable.dispose()
                        }
                    }

                    override fun onError(e: Throwable) {
                    }
                })
    }
}
