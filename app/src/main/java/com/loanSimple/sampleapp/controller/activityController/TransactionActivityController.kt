package com.loanSimple.sampleapp.controller.activityController

import com.loanSimple.sampleapp.viewmodel.activityViewModel.TransactionActivityViewModel
import com.loanSimple.sampleapp.daggerPack.DaggerTransactionActivityControllerComponent
import javax.inject.Inject

class TransactionActivityController @Inject constructor() {
    private var transactionActivityInterctor = DaggerTransactionActivityControllerComponent.create().getInteractor()
    private var transactionActivityPresenter = DaggerTransactionActivityControllerComponent.create().getPresenter()
    private var dateListInteractor = DaggerTransactionActivityControllerComponent.create().getDateInteractor()
    private var dateListPresenter = DaggerTransactionActivityControllerComponent.create().getDatePresenter()

    fun sendTransactionRequest(accountId: Int, sessionId: String, viewModel: TransactionActivityViewModel) {
        transactionActivityPresenter.setTransactionRequestResponse(
            transactionActivityInterctor.getTransactionResponseFromGateway(
                accountId,
                sessionId
            ), viewModel
        )
    }

    fun sendDateListRequest(viewModel: TransactionActivityViewModel) {
        dateListPresenter.setDateListObservable(viewModel, dateListInteractor.getDateList())
    }

}