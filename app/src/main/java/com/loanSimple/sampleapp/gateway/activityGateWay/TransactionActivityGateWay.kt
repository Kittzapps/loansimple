package com.loanSimple.sampleapp.gateway.activityGateWay

import com.loanSimple.sampleapp.models.response.UserUpiTranscationModel
import io.reactivex.Observable

interface TransactionActivityGateWay {
    fun observeTransactionRequestResponse(accountId: Int, sessionId: String):Observable<UserUpiTranscationModel>
}