package com.loanSimple.sampleapp.daggerPack

import com.loanSimple.sampleapp.interactor.activityInteractor.TransactionActivityInterctor
import com.loanSimple.sampleapp.presenter.activityPresenter.TransactionActivityPresenter
import com.loanSimple.sampleapp.presenter.DateListPresenter
import com.loanSimple.sampleapp.interactor.DateListInteractor
import dagger.Component

@Component(modules = [TransactionActivityControllerModuel::class,DateListGatewayModule::class])
interface TransactionActivityControllerComponent {
    fun getInteractor():TransactionActivityInterctor
    fun getPresenter():TransactionActivityPresenter
    fun getDateInteractor():DateListInteractor
    fun getDatePresenter():DateListPresenter
}