package com.loanSimple.sampleapp.networkClient

import com.loanSimple.sampleapp.models.requestPayload.OtpVerificationPayload
import com.loanSimple.sampleapp.models.requestPayload.RequestOtpPayload
import com.loanSimple.sampleapp.models.response.OtpVerificationResponseModel
import com.loanSimple.sampleapp.models.response.SendOtpModel
import com.loanSimple.sampleapp.models.response.UserUpiTranscationModel
import retrofit2.Call
import retrofit2.http.*

interface GetApis {


    @POST("api/v2/merchant/login/")
    fun loginCall(@Body requestPayload: RequestOtpPayload): Call<SendOtpModel>

    @POST("api/v2/merchant/login/")
    fun otpVerificationCall(@Body requestPayload: OtpVerificationPayload): Call<OtpVerificationResponseModel>

    @GET("api/v2/pesimple/accounts/{accountId}/upi_transactions/")
    fun upiTranscationCall(@Path("accountId") accountId: Int, @Header("Authorization") Authorization:String): Call<UserUpiTranscationModel>

}