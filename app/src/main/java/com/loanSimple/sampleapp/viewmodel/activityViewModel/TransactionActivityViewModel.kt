package com.loanSimple.sampleapp.viewmodel.activityViewModel

import com.loanSimple.sampleapp.models.response.UserUpiTranscationModel
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import java.util.*
import javax.inject.Inject

class TransactionActivityViewModel @Inject constructor() {
    private var observableTransactionResponse: BehaviorSubject<UserUpiTranscationModel> = BehaviorSubject.create()
    private var observableDateList: BehaviorSubject<List<Date>> = BehaviorSubject.create()

    fun getTransactionResponseObservable(): Observable<UserUpiTranscationModel> {
        return observableTransactionResponse
    }

    fun setTransactionResponseObservable(responseFromGateway: UserUpiTranscationModel) {
        observableTransactionResponse.onNext(responseFromGateway)
    }
    fun getDateListObservable(): Observable<List<Date>> {
        return observableDateList
    }

    fun setDateListObservable(dateList: List<Date>) {
        observableDateList.onNext(dateList)
    }
}


