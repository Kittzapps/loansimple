package com.loanSimple.sampleapp.interactor.activityInteractor

import com.loanSimple.sampleapp.gateway.activityGateWay.LoginActivityGateWay
import com.loanSimple.sampleapp.models.response.SendOtpModel
import io.reactivex.Observable
import javax.inject.Inject

class LoginActivityInterctor @Inject constructor(private var loginActivityGateWay: LoginActivityGateWay) {
    fun getResponseFromGateway(number : String): Observable<SendOtpModel> {
        return loginActivityGateWay.observeOtpRequestResponse(number)
    }
}