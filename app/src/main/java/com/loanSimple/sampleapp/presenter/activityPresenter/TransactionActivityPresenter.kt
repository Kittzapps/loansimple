package com.loanSimple.sampleapp.presenter.activityPresenter

import android.util.Log
import com.loanSimple.sampleapp.viewmodel.activityViewModel.TransactionActivityViewModel
import com.loanSimple.sampleapp.models.response.UserUpiTranscationModel
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class TransactionActivityPresenter @Inject constructor() {
    lateinit var disposable: Disposable
    fun setTransactionRequestResponse(
        responseFromGateway: Observable<UserUpiTranscationModel>,
        viewModel: TransactionActivityViewModel
    ) {
        responseFromGateway.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<UserUpiTranscationModel> {
                override fun onComplete() {
                }

                override fun onSubscribe(d: Disposable) {
                    disposable = d
                }

                override fun onNext(t: UserUpiTranscationModel) {
                    Log.d("BHANU","INSIDE PRESE HISTORY---> $t")
                    viewModel.setTransactionResponseObservable(t)
                    disposable.dispose()
                }

                override fun onError(e: Throwable) {
                }

            })

    }

}