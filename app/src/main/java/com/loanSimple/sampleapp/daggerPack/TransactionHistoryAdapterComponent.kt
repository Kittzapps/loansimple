package com.loanSimple.sampleapp.daggerPack

import com.loanSimple.sampleapp.adapter.TransactionHistoryAdapter
import com.loanSimple.sampleapp.models.response.ResultsItem
import dagger.BindsInstance
import dagger.Component

@Component
interface TransactionHistoryAdapterComponent {
    fun getTransactionAdapter(): TransactionHistoryAdapter

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun getTransData(transactionList: List<ResultsItem>): Builder

        fun build(): TransactionHistoryAdapterComponent
    }
}