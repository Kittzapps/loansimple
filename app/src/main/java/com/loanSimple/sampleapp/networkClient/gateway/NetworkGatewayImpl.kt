package com.loanSimple.sampleapp.networkClient.gateway

import com.loanSimple.sampleapp.networkClient.GetApis
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class NetworkGatewayImpl @Inject constructor() : NetworkGateWay {
    override fun getRetrofit(): GetApis {
        return getClient()
    }

   private fun getClient(): GetApis {
        return Retrofit.Builder()
            .baseUrl("http://stagls.loansimple.in/")
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(GetApis::class.java)
    }
}