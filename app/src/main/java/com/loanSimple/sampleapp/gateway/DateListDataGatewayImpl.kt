package com.loanSimple.sampleapp.gateway

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import java.util.*
import javax.inject.Inject

class DateListDataGatewayImpl @Inject constructor() : DateListDataGateway {
    private var observeDates: BehaviorSubject<List<Date>> = BehaviorSubject.create()
    override fun getDates(startDate: Date, endDate: Date): Observable<List<Date>> {
        return getDateObservable(startDate, endDate)
    }

    private fun getDateList(startDate: Date, endDate: Date): Observable<List<Date>> {
        val datesInRange = ArrayList<Date>()
        val calendar = GregorianCalendar()
        calendar.time = startDate
        val endCalendar = GregorianCalendar()
        endCalendar.time = endDate
        while (calendar.before(endCalendar)) {
            val result = calendar.time
            datesInRange.add(result)
            calendar.add(Calendar.DATE, 1)
        }
        setDataInObservable(datesInRange)
        return observeDates
    }

    private fun setDataInObservable(dates: List<Date>) {
        observeDates.onNext(dates)
    }

    private fun getDateObservable(startDate: Date, endDate: Date): Observable<List<Date>> {
        return getDateList(startDate, endDate)
    }

}
