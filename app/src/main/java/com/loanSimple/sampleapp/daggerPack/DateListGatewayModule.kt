package com.loanSimple.sampleapp.daggerPack

import com.loanSimple.sampleapp.gateway.DateListDataGateway
import com.loanSimple.sampleapp.gateway.DateListDataGatewayImpl
import dagger.Module
import dagger.Provides

@Module
class DateListGatewayModule {
    @Provides
    fun getDateGateway(): DateListDataGateway {
        return DateListDataGatewayImpl()
    }
}