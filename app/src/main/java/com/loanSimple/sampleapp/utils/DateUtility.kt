package com.loanSimple.sampleapp.utils

import java.util.*

class DateUtility{
    companion object{
         fun getOneMonthDates(): Date {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.MONTH, 1)
            return calendar.time
        }

         fun getCurrentDate(): Date {
            return Calendar.getInstance().time
        }
    }

}