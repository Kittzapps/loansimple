package com.loanSimple.sampleapp.gatewayImpl.activityGatewayImpl.fragmentGatewayImpl

import android.util.Log
import com.loanSimple.sampleapp.daggerPack.DaggerNetworkComponnet
import com.loanSimple.sampleapp.gateway.fragmentGateway.OtpVerificationGateway
import com.loanSimple.sampleapp.models.response.OtpVerificationResponseModel
import com.loanSimple.sampleapp.models.requestPayload.OtpVerificationPayload
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class OtpVerificationGatewayImpl @Inject constructor() :
    OtpVerificationGateway {
    private val behaviourSubject: BehaviorSubject<OtpVerificationResponseModel> = BehaviorSubject.create()

    private fun setVerificationObservableData(otpModel: OtpVerificationResponseModel) {
        behaviourSubject.onNext(otpModel)
    }

    private fun getVerificationObservableData(otp: String,mobile: String): Observable<OtpVerificationResponseModel> {
        return getResponseFromOtpVerificationRequest(otp,mobile)
    }

    override fun getOtpVerificationResponse(otp: String,mobile: String): Observable<OtpVerificationResponseModel> {
        return getVerificationObservableData(otp,mobile)
    }

    private fun getResponseFromOtpVerificationRequest(otp: String,mobile : String): Observable<OtpVerificationResponseModel> {
        Log.d("BHANU","NUMBER::: $mobile $otp")
        DaggerNetworkComponnet.create().getNetworkClient().getApiClient().otpVerificationCall(OtpVerificationPayload(mobile, "verify_otp",true,otp))
            .enqueue(object : Callback<OtpVerificationResponseModel> {
                override fun onFailure(call: Call<OtpVerificationResponseModel>, t: Throwable) {

                }

                override fun onResponse(call: Call<OtpVerificationResponseModel>, response: Response<OtpVerificationResponseModel>) {
                    if (response.body() != null) {
                        setVerificationObservableData(response.body()!!)
                    }
                }

            })
        return behaviourSubject
    }
}