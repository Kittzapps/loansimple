package com.loanSimple.sampleapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.loanSimple.sampleapp.R
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class DateAdapterHorizontal @Inject constructor(private var listOfDates: List<Date>) :
    RecyclerView.Adapter<DateAdapterHorizontal.DateViewHolder>() {
    private var tempPosition: Int = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DateViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.date_item, parent, false)
        return DateViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listOfDates.size
    }

    override fun onBindViewHolder(holder: DateViewHolder, position: Int) {
        holder.dayTextView.setOnClickListener {
            tempPosition = position
            holder.dateTextView.setBackgroundResource(R.drawable.circle_green)
            notifyDataSetChanged()
        }
        if (tempPosition == position) {
            holder.dateTextView.setBackgroundResource(R.drawable.circle_green)
            holder.dateTextView.setTextColor(holder.itemView.context.resources.getColor(R.color.colorWhite))
        } else {
            holder.dateTextView.setBackgroundResource(0)
            holder.dateTextView.setTextColor(holder.itemView.context.resources.getColor(R.color.colorBlack))
        }

        val outFormat = SimpleDateFormat("EEE")
        val goal = outFormat.format(listOfDates[position])

        if (position == 0) {
            holder.dayTextView.text = "Today"
        } else {
            holder.dayTextView.text = goal
        }
        holder.dateTextView.text = listOfDates[position].date.toString()
    }

    class DateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var dayTextView = itemView.findViewById<TextView>(R.id.dayTextView)!!
        var dateTextView = itemView.findViewById<TextView>(R.id.dateTextView)!!
    }
}