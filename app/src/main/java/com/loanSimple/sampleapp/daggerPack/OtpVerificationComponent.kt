package com.loanSimple.sampleapp.daggerPack

import com.loanSimple.sampleapp.controller.activityController.fragmentController.OtpVerificationController
import com.loanSimple.sampleapp.viewmodel.activityViewModel.fragmentViewModel.OtpVerificationViewModel
import dagger.Component

@Component
interface OtpVerificationComponent {
    fun otpVerificationController(): OtpVerificationController
    fun otpVerificationViewModel(): OtpVerificationViewModel
}