package com.loanSimple.sampleapp.daggerPack

import com.loanSimple.sampleapp.adapter.DateAdapterHorizontal
import dagger.BindsInstance
import dagger.Component
import java.util.*

@Component
interface HorizontalDateAdapterComponent {
    fun getDateAdapter(): DateAdapterHorizontal

    @Component.Builder
    interface Builder{

        @BindsInstance
        fun getListData(dateList:List<Date>):Builder

        fun build():HorizontalDateAdapterComponent
    }
}