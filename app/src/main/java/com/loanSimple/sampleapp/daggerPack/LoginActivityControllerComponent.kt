package com.loanSimple.sampleapp.daggerPack

import com.loanSimple.sampleapp.interactor.activityInteractor.LoginActivityInterctor
import com.loanSimple.sampleapp.presenter.activityPresenter.LoginActivityPresenter
import dagger.Component

@Component(modules = [LoginActivityControllerModuel::class])
interface LoginActivityControllerComponent {
    fun getLoginActivityInteractor():LoginActivityInterctor
    fun getLoginActivityPresenter():LoginActivityPresenter
}