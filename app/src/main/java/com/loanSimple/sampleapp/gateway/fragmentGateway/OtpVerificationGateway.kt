package com.loanSimple.sampleapp.gateway.fragmentGateway

import com.loanSimple.sampleapp.models.response.OtpVerificationResponseModel
import io.reactivex.Observable

interface OtpVerificationGateway {
    fun getOtpVerificationResponse(otp: String,mobile: String): Observable<OtpVerificationResponseModel>
}