package com.loanSimple.sampleapp.gateway.activityGateWay

import com.loanSimple.sampleapp.models.response.SendOtpModel
import io.reactivex.Observable

interface LoginActivityGateWay {
    fun observeOtpRequestResponse(number: String):Observable<SendOtpModel>
}