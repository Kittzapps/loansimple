package com.loanSimple.sampleapp.gatewayImpl.activityGatewayImpl

import android.util.Log
import com.loanSimple.sampleapp.gateway.activityGateWay.TransactionActivityGateWay
import com.loanSimple.sampleapp.daggerPack.DaggerNetworkComponnet
import com.loanSimple.sampleapp.models.response.UserUpiTranscationModel
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class TransactionActivityGatewayImpl @Inject constructor() : TransactionActivityGateWay {
    override fun observeTransactionRequestResponse(accountId: Int, sessionId: String): Observable<UserUpiTranscationModel> {
        return getTransactionObservable(accountId,sessionId)    }

    private val behaviourSubject: BehaviorSubject<UserUpiTranscationModel> = BehaviorSubject.create()

    private fun setObservableData(userUpiTranscationModel: UserUpiTranscationModel) {
        behaviourSubject.onNext(userUpiTranscationModel)
    }

    private fun getTransactionObservable(accountId: Int, sessionId: String): Observable<UserUpiTranscationModel> {
        return getResponseFromTransactionRequest(accountId,sessionId)
    }


    private fun getResponseFromTransactionRequest(accountId: Int, sessionId: String): Observable<UserUpiTranscationModel> {
        DaggerNetworkComponnet.create().getNetworkClient().getApiClient()
            .upiTranscationCall(accountId,sessionId)
            .enqueue(object : Callback<UserUpiTranscationModel> {
                override fun onFailure(call: Call<UserUpiTranscationModel>, t: Throwable) {

                }

                override fun onResponse(call: Call<UserUpiTranscationModel>, response: Response<UserUpiTranscationModel>) {
                    if (response.body() != null) {
                        Log.d("BHANU", "INSIDE IMPL ACTIVITY---->")
                        setObservableData(response.body()!!)

                    }
                }

            })
        return behaviourSubject
    }

}