package com.loanSimple.sampleapp.viewmodel.activityViewModel

import com.loanSimple.sampleapp.models.response.SendOtpModel
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class LoginActivityViewModel @Inject constructor() {
     var observableOtpResponse: BehaviorSubject<SendOtpModel> = BehaviorSubject.create()

    fun getOtpResponseObservable(): Observable<SendOtpModel> {
        return observableOtpResponse
    }

    fun setOtpResponseObservable(responseFromGateway: SendOtpModel) {
        observableOtpResponse.onNext(responseFromGateway)
    }
}