package com.loanSimple.sampleapp.binder.activityBinder

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.snackbar.Snackbar
import com.loanSimple.sampleapp.R
import com.loanSimple.sampleapp.controller.activityController.LoginActivityController
import com.loanSimple.sampleapp.viewmodel.activityViewModel.LoginActivityViewModel
import com.loanSimple.sampleapp.daggerPack.DaggerMainActivityComponent
import com.loanSimple.sampleapp.binder.fragmentBinder.OTPVerify
import com.loanSimple.sampleapp.models.response.SendOtpModel
import com.loanSimple.sampleapp.utils.Logger
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import javax.inject.Inject
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.ViewAnimationUtils
import android.opengl.ETC1.getHeight
import android.opengl.ETC1.getWidth
import android.view.animation.AccelerateDecelerateInterpolator




class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var loginActivityController: LoginActivityController
    @Inject
    lateinit var loginActivityViewModel: LoginActivityViewModel
    lateinit var disposable: Disposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initDependencies()
        userAction()
    }

    private fun initDependencies() {
        loginActivityController = DaggerMainActivityComponent.create().getController()
    }

    private fun checkCondition(): String? {
        return if (userPhoneNumberEditText.text.toString().isEmpty() || userPhoneNumberEditText.text.toString().length < 10) {
            val snackbar = Snackbar
                .make(main, "Please enter a valid Number", Snackbar.LENGTH_LONG)
            snackbar.show()
            null
        } else
            userPhoneNumberEditText.text.toString()
    }

    fun userAction() {
        userPhoneNumberEditText.setText("9968979264")
        sendOtpTextView.setOnClickListener {
            loginActivityViewModel = DaggerMainActivityComponent.create().getViewModel()
            loginActivityController.sendOtpRequest(
                checkCondition()!!,
                loginActivityViewModel!!
            )
            observeOtpRequest()
        }
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    private fun observeOtpRequest() {
        // Log.d("BHANU", "INSTANSE OF obser----> $loginActivityViewModel")
        if (::loginActivityViewModel.isInitialized) {

        }
        loginActivityViewModel.getOtpResponseObservable().subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())?.subscribe(
                object : Observer<SendOtpModel> {
                    override fun onComplete() {
                        disposable.dispose()
                    }

                    override fun onSubscribe(d: Disposable) {
                        disposable = d
                    }

                    override fun onNext(t: SendOtpModel) {
                        Logger.d("BHANU", "RESPONSE -----------> $t")
                        openOtpVerifer(userPhoneNumberEditText.text.toString())
                        disposable.dispose()

                    }

                    override fun onError(e: Throwable) {
                    }
                })
    }

    private fun openOtpVerifer(number: String) {
        val fragmentTrans: FragmentTransaction = supportFragmentManager.beginTransaction()
        val fragment = OTPVerify()
        val bundle = Bundle()
        bundle.putString("number", number)
        fragment.arguments = bundle
        fragmentTrans.replace(com.loanSimple.sampleapp.R.id.main, fragment)
        fragmentTrans.addToBackStack("OTP_VERIFY")
        fragmentTrans.commit()
    }



}
