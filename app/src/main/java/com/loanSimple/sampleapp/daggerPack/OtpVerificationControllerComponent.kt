package com.loanSimple.sampleapp.daggerPack

import com.loanSimple.sampleapp.interactor.fragmentInteractor.OtpVerificationInteractor
import com.loanSimple.sampleapp.presenter.fragmentPresenter.OtpVerificationPresenter
import dagger.Component

@Component(modules = [OtpVerificationGatewayModule::class])
interface OtpVerificationControllerComponent {
    fun getInteractor(): OtpVerificationInteractor
    fun getPresenter(): OtpVerificationPresenter
}