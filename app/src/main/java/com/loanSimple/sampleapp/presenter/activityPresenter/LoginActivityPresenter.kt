package com.loanSimple.sampleapp.presenter.activityPresenter

import android.util.Log
import com.loanSimple.sampleapp.viewmodel.activityViewModel.LoginActivityViewModel
import com.loanSimple.sampleapp.models.response.SendOtpModel
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class LoginActivityPresenter @Inject constructor() {
    lateinit var disposable: Disposable
    fun setOtpRequestResponse(
        responseFromGateway: Observable<SendOtpModel>,
        viewModel: LoginActivityViewModel
    ) {
        responseFromGateway.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<SendOtpModel> {
                override fun onComplete() {
                }

                override fun onSubscribe(d: Disposable) {
                    disposable = d
                }

                override fun onNext(t: SendOtpModel) {
                    Log.d("BHANU","INSTANSE INSIDE PRESE---> $viewModel  $t")
                    viewModel.setOtpResponseObservable(t)
                    disposable.dispose()
                }

                override fun onError(e: Throwable) {
                }

            })

    }

}