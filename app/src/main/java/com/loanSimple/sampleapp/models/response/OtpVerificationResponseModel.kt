package com.loanSimple.sampleapp.models.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable


data class OtpVerificationResponseModel(
    @SerializedName("found")
    val found: Boolean = false,
    @SerializedName("data")
    val data: Data
) : Serializable


data class Data(
    @SerializedName("application_id")
    val applicationId: String = "",
    @SerializedName("pesimple_account_id")
    val pesimpleAccountId: Int = 0,
    @SerializedName("loan_account_id")
    val loanAccountId: String = "",
    @SerializedName("lead_id")
    val leadId: Int = 0,
    @SerializedName("business_id")
    val businessId: Int = 0,
    @SerializedName("business_profile_photo")
    val businessProfilePhoto: String = "",
    @SerializedName("business_name")
    val businessName: String = "",
    @SerializedName("pesimple_status")
    val pesimpleStatus: String = "",
    @SerializedName("mobile")
    val mobile: String = "",
    @SerializedName("email")
    val email: String = "",
    @SerializedName("person_name")
    val personName: String = "",
    @SerializedName("interested_for_pesimple")
    val interestedForPesimple: String = "",
    @SerializedName("interested_for_loan")
    val interestedForLoan: String = "",
    @SerializedName("sessionid")
    val sessionid: String = ""
) : Serializable


