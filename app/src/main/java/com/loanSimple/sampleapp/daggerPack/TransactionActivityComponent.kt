package com.loanSimple.sampleapp.daggerPack

import com.loanSimple.sampleapp.controller.activityController.TransactionActivityController
import com.loanSimple.sampleapp.viewmodel.activityViewModel.TransactionActivityViewModel
import dagger.Component

@Component
interface TransactionActivityComponent {
    fun getController(): TransactionActivityController
    fun getViewModel(): TransactionActivityViewModel
}