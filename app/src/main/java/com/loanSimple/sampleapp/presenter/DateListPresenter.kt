package com.loanSimple.sampleapp.presenter

import com.loanSimple.sampleapp.viewmodel.activityViewModel.TransactionActivityViewModel
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class DateListPresenter @Inject constructor() {
    private lateinit var disposable: Disposable
    fun setDateListObservable(viewModel: TransactionActivityViewModel, observable: Observable<List<Date>>) {
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(object :
            Observer<List<Date>> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
                disposable = d
            }

            override fun onNext(t: List<Date>) {
                viewModel.setDateListObservable(t)
                disposable.dispose()

            }

            override fun onError(e: Throwable) {
            }
        })

    }
}