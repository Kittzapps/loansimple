package com.loanSimple.sampleapp.interactor.fragmentInteractor

import com.loanSimple.sampleapp.gateway.fragmentGateway.OtpVerificationGateway
import com.loanSimple.sampleapp.models.response.OtpVerificationResponseModel
import io.reactivex.Observable
import javax.inject.Inject

class OtpVerificationInteractor @Inject constructor(var otpVerificationGateway: OtpVerificationGateway) {
    fun getResponseFromOtpVerificationGateway(otp : String,mobile: String): Observable<OtpVerificationResponseModel> {
        return otpVerificationGateway.getOtpVerificationResponse(otp,mobile)
    }
}