package com.loanSimple.sampleapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.loanSimple.sampleapp.R
import com.loanSimple.sampleapp.models.response.ResultsItem
import android.graphics.Color
import java.util.*
import javax.inject.Inject


class TransactionHistoryAdapter @Inject constructor(val transactionList: List<ResultsItem>) :
    RecyclerView.Adapter<TransactionHistoryAdapter.TransactionViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.transaction_item, parent, false)
        return TransactionViewHolder(view)
    }

    override fun getItemCount(): Int {
        return transactionList.size
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.amtTextView.text = "+ ".plus(transactionList[position].amount)
        holder.payerNameTextView.text = transactionList[position].payeeName
        holder.profileImageContainer.setBackgroundResource(R.drawable.circle)
        val rnd = Random()
        val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
        holder.image.setBackgroundColor(color)
    }

    class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var amtTextView = itemView.findViewById<TextView>(R.id.amtTextView)
        var payerNameTextView = itemView.findViewById<TextView>(R.id.payerNameTextView)
        var profileImageContainer = itemView.findViewById<CardView>(R.id.profileImageContainer)
        var image = itemView.findViewById<ImageView>(R.id.image)

    }
}