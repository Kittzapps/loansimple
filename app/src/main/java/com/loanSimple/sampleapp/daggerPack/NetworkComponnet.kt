package com.loanSimple.sampleapp.daggerPack

import com.loanSimple.sampleapp.networkClient.gateway.NetworkInteractor
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkClientModule::class])
interface NetworkComponnet {
    fun getNetworkClient(): NetworkInteractor
}