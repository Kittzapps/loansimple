package com.loanSimple.sampleapp.daggerPack

import com.loanSimple.sampleapp.controller.activityController.LoginActivityController
import com.loanSimple.sampleapp.viewmodel.activityViewModel.LoginActivityViewModel
import dagger.Component

@Component
interface MainActivityComponent {
    fun getController(): LoginActivityController
    fun getViewModel(): LoginActivityViewModel
}